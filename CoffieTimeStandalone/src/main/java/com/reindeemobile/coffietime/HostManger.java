package com.reindeemobile.coffietime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.reindeemobile.coffietime.network.CoffieTimeServer;

public class HostManger {
	private final Map<String, Node> nodes = new HashMap<>();
	private CoffieTimeServer coffieTimeServer;
	private PropertyChangeListener receivePairRequest = new PropertyChangeListener() {
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getPropertyName().equals("")) {
				Node node = (Node) evt.getNewValue();
				
			}
		}
	};

	public HostManger() {
		super();
		/*
		 * Start server thread.
		 */
		// this.coffieTimeServer = new CoffieTimeServer("test-host-1", 34567,
		// 34566);
		this.coffieTimeServer = new CoffieTimeServer("test-host-2", 34566,
				34567);
		Thread coffieTimeServerThread = new Thread(coffieTimeServer);
		coffieTimeServerThread.start();

	}

	public Node add(final String hostIp) {
		Node newNode = new Node(hostIp);
		this.nodes.put(hostIp, newNode);
		coffieTimeServer.sendPairRequest(newNode);
		return newNode;
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}
	
	public Node findByIp(String ip) {
		return this.nodes.get(ip);
	}

	public void close() {
		if (coffieTimeServer != null) {
			coffieTimeServer.close();
		}
	}

}
