package com.reindeemobile.coffietime;

/**
 * Egy Node elemet reprezentál. Ez tárol egy nickname-ip cím párost.
 */
public class Node {
	private String ip;
	private String name;
	private NodeStatus nodeStatus;

	public Node(String ip) {
		super();
		this.ip = ip;
		this.nodeStatus = NodeStatus.ADDED;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NodeStatus getNodeStatus() {
		return nodeStatus;
	}

	public void setNodeStatus(NodeStatus nodeStatus) {
		this.nodeStatus = nodeStatus;
	}
}