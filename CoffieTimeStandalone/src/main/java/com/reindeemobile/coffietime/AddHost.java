package com.reindeemobile.coffietime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Label;

/**
 * Dialog for add new host.
 */
public class AddHost extends Dialog {

	protected Object result;
	protected Shell shlAddHost;
	private Text text;
	
	PropertyChangeListener changeListener;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public AddHost(final Shell parent, final int style) {
		super(parent, style);
		setText("SWT Dialog");
	}
	
	public void setChangeListener(PropertyChangeListener changeListener) {
		this.changeListener = changeListener;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public final Object open() {
		createContents();
		shlAddHost.open();
		shlAddHost.layout();
		Display display = getParent().getDisplay();
		while (!shlAddHost.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlAddHost = new Shell(getParent(), SWT.DIALOG_TRIM
				| SWT.APPLICATION_MODAL);
		shlAddHost.setSize(363, 145);
		shlAddHost.setText("Add host");
		shlAddHost.setLayout(new FormLayout());

		Label lblIp = new Label(shlAddHost, SWT.NONE);
		FormData fd_lblIp = new FormData();
		fd_lblIp.top = new FormAttachment(0, 10);
		fd_lblIp.left = new FormAttachment(0, 10);
		lblIp.setLayoutData(fd_lblIp);
		lblIp.setText("Target IP address:");

		text = new Text(shlAddHost, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.bottom = new FormAttachment(lblIp, 36, SWT.BOTTOM);
		fd_text.right = new FormAttachment(0, 351);
		fd_text.top = new FormAttachment(lblIp, 6);
		fd_text.left = new FormAttachment(0, 10);
		text.setLayoutData(fd_text);

		Button btnNewButton = new Button(shlAddHost, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (changeListener != null) {
					changeListener.propertyChange(new PropertyChangeEvent(this, CoffieTimeWindow.ADD_NODE_IP, null, text.getText()));
				}
				shlAddHost.close();
			}
		});
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.left = new FormAttachment(100, -89);
		fd_btnNewButton.top = new FormAttachment(text, 6);
		fd_btnNewButton.right = new FormAttachment(100, -10);
		btnNewButton.setLayoutData(fd_btnNewButton);
		btnNewButton.setText("OK");

	}

}
