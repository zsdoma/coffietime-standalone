package com.reindeemobile.coffietime.network.json;

import java.util.Map;

public class Request {
	public static enum RequestType {
		PAIR_REQUEST, PAIR_REQUEST_ACCEPTED, COFFIE_TIME_REQUEST, COFFIE_TIME_ACCEPTED;
	}
	private RequestType type;
	private Map<String, String> properties;

	public RequestType getType() {
		return type;
	}

	public void setType(RequestType type) {
		this.type = type;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		return "Request [type=" + type + ", properties=" + properties + "]";
	}

}
