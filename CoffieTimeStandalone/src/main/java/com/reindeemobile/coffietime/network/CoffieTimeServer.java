package com.reindeemobile.coffietime.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.reindeemobile.coffietime.Node;
import com.reindeemobile.coffietime.network.json.Request;
import com.reindeemobile.coffietime.network.json.Request.RequestType;

/**
 * CoffieTimeServer class.
 */
public class CoffieTimeServer implements Runnable {
	private static final String LOCALHOST_IP = "127.0.0.1";

	/**
	 * Server port.
	 */
	private static final int SERVER_PORT = 34567;
	private static final int LISTENER_PORT = 34567;

	/**
	 * Logger.
	 */
	private static Logger logger = LoggerFactory
			.getLogger(CoffieTimeServer.class);

	/**
	 * {@link ServerSocket}.
	 */
	private ServerSocket serverSocket;

	/**
	 * {@link Gson} for JSON parsing.
	 */
	private Gson gson = new Gson();

	private String name = "noname";

	private int serverPort = SERVER_PORT;
	private int listenerPort = LISTENER_PORT;

	public CoffieTimeServer(String name, int listenerPort, int serverPort) {
		super();
		this.name = name;
		this.listenerPort = listenerPort;
		this.serverPort = serverPort;
	}

	@Override
	public void run() {
		logger.info("Server thread started.");
		BufferedReader bufferedReader = null;
		Socket acceptedSocket = null;
		try {
			serverSocket = new ServerSocket(listenerPort);
			while ((acceptedSocket = serverSocket.accept()) != null) {
				InputStream inputStream = acceptedSocket.getInputStream();
				logger.info("accepted from: "
						+ acceptedSocket.getInetAddress().getHostAddress());
				bufferedReader = new BufferedReader(new InputStreamReader(
						inputStream));
				String readLine = bufferedReader.readLine();
				logger.debug(readLine);
				Request request = gson.fromJson(readLine, Request.class);
				logger.debug(request.toString());
				receive(request);
			}
		} catch (IOException e) {
			logger.error("IOError", e);
		} finally {
			if (acceptedSocket != null) {
				try {
					acceptedSocket.close();
				} catch (IOException e) {
					logger.error("acceptedSocket close error", e);
				}
			}
		}
		logger.info("Server thread stopped.");
	}

	private void receive(Request request) {
		if (RequestType.PAIR_REQUEST == request.getType()) {
			// TODO 
		} else if (RequestType.COFFIE_TIME_REQUEST == request.getType()) {
			// TODO
		} else if (RequestType.COFFIE_TIME_ACCEPTED == request.getType()) {
			// TODO
		} else if (RequestType.PAIR_REQUEST_ACCEPTED == request.getType()) {
			// TODO
		}
	}

	private void send(String host, String data) {
		OutputStream outputStream = null;
		Socket socket = null;
		try {
			socket = new Socket(host, serverPort);
			outputStream = socket.getOutputStream();
			outputStream.write(data.getBytes());
			outputStream.flush();
		} catch (IOException e) {
			logger.error("IOError", e);
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					logger.error("IOError", e);
				}
			}
		}

	}

	public Node sendPairRequest(Node node) {
		Request request = new Request();
		request.setType(RequestType.PAIR_REQUEST);
		Map<String, String> map = new HashMap<>();
		map.put("src_host", LOCALHOST_IP);
		map.put("dest_host", node.getIp());
		map.put("src_name", name);
		request.setProperties(map);
		send(node.getIp(), gson.toJson(request));
		return node;
	}

	public Node sendCoffieTimeRequest(Node node) {
		long currentTimeMillis = System.currentTimeMillis();
		Request request = new Request();
		request.setType(RequestType.COFFIE_TIME_REQUEST);
		Map<String, String> map = new HashMap<>();
		map.put("src_host", LOCALHOST_IP);
		map.put("timestamp",
				String.valueOf(currentTimeMillis + (5 * 60 * 1000)));
		send(node.getIp(), gson.toJson(request));
		return null;
	}

	/**
	 * Try to close server socket.
	 */
	public final void close() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				logger.error("IOError", e);
			}
		}
	}

}
