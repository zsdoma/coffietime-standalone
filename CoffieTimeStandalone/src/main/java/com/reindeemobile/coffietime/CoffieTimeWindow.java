package com.reindeemobile.coffietime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.reindeemobile.coffietime.network.CoffieTimeServer;

public class CoffieTimeWindow implements ShellListener {
	public static final String ADD_NODE_IP = "addNodeIp";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CoffieTimeWindow.class);

	protected Shell shlCoffieTime;
//	private CoffieTimeServer coffieTimeServer;
	private HostManger hostManger;

	private java.util.List<PropertyChangeListener> changeListeners;

	private List list;

	public static void main(String[] args) {
		try {
			CoffieTimeWindow window = new CoffieTimeWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		hostManger = new HostManger();
		/*
		 * Start server thread.
		 */
//		this.coffieTimeServer = new CoffieTimeServer("test-host-1", 34567, 34566);
//		this.coffieTimeServer = new CoffieTimeServer("test-host-2", 34566, 34567);
//		Thread coffieTimeServerThread = new Thread(coffieTimeServer);
//		coffieTimeServerThread.start();

		/*
		 * Setup GUI.
		 */
		Display display = Display.getDefault();
		createContents();
		shlCoffieTime.addShellListener(this);
		shlCoffieTime.open();
		shlCoffieTime.layout();
		while (!shlCoffieTime.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCoffieTime = new Shell();
		shlCoffieTime.setSize(257, 337);
		shlCoffieTime.setText("Coffie Time");
		shlCoffieTime.setLayout(new FormLayout());

		final Button btnCoffieTime = new Button(shlCoffieTime, SWT.NONE);
		FormData fd_btnCoffieTime = new FormData();
		btnCoffieTime.setLayoutData(fd_btnCoffieTime);
		btnCoffieTime.setText("Coffie Time");

		final Button btnAdd = new Button(shlCoffieTime, SWT.NONE);
		fd_btnCoffieTime.top = new FormAttachment(0, 44);

		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (e.widget.equals(btnAdd)) {
					showAddHostDialog();
				} else if (e.widget.equals(btnCoffieTime)) {
					LOGGER.info("CoffieTime!");
				}
			}
		};

		btnCoffieTime.addSelectionListener(selectionAdapter);
		btnAdd.addSelectionListener(selectionAdapter);

		FormData fd_btnAdd = new FormData();
		fd_btnAdd.right = new FormAttachment(btnCoffieTime, 0, SWT.RIGHT);
		fd_btnAdd.bottom = new FormAttachment(btnCoffieTime, -6);
		fd_btnAdd.left = new FormAttachment(btnCoffieTime, 0, SWT.LEFT);
		btnAdd.setLayoutData(fd_btnAdd);
		btnAdd.setText("Paired User");

		list = new List(shlCoffieTime, SWT.BORDER | SWT.V_SCROLL);
		fd_btnCoffieTime.left = new FormAttachment(0, 168);
		list.setItems(new String[] {});
		FormData fd_list = new FormData();
		fd_list.right = new FormAttachment(btnCoffieTime, -6);
		fd_list.left = new FormAttachment(0, 10);
		fd_list.bottom = new FormAttachment(100, -10);
		fd_list.top = new FormAttachment(0, 10);
		list.setLayoutData(fd_list);

	}

	@Override
	public void shellActivated(final ShellEvent arg0) {
	}

	@Override
	public final void shellClosed(final ShellEvent arg0) {
		if (hostManger != null) {
			hostManger.close();
		}
	}

	@Override
	public void shellDeactivated(final ShellEvent arg0) {
	}

	@Override
	public void shellDeiconified(final ShellEvent arg0) {
	}

	@Override
	public void shellIconified(final ShellEvent arg0) {
	}

	/**
	 * Show {@link AddHost} dialog.
	 */
	private final void showAddHostDialog() {
		AddHost newHost = new AddHost(shlCoffieTime, SWT.SHELL_TRIM);
		newHost.setChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals(ADD_NODE_IP)) {
					String string = (String) evt.getNewValue();
					list.add(string);
					Node node = hostManger.add(string);
				}
			}
		});
		newHost.open();
	}
}
